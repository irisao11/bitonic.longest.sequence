package longest.betonic.sequence;

import java.util.ArrayList;
import java.util.List;


public class LongestBetonicSequence {
    public void findLongestBetonicSequence(List<Integer> myList) {

        List<Integer> sequenceList = new ArrayList<>();
        List<List<Integer>> listOfSequences = new ArrayList<>();
        Integer high = 0, low = myList.get(0);
        for (int i = 1; i < myList.size() - 1; i++) {
            if (myList.get(i - 1) < myList.get(i) && myList.get(i) < myList.get(i + 1)) {
                sequenceList.add(myList.get(i));
                System.out.println(sequenceList.toString());
            }
            if (myList.get(i - 1) < myList.get(i) && myList.get(i) > myList.get(i + 1)) {
                high = myList.get(i);
                System.out.println("high " + high);
                sequenceList.add(high);
                System.out.println(sequenceList);
            }
            if (myList.get(i - 1) > myList.get(i) && myList.get(i) > myList.get(i + 1)) {
                sequenceList.add(myList.get(i));
                System.out.println(sequenceList);
            }
            if (myList.get(i - 1) > myList.get(i) && myList.get(i) < myList.get(i + 1)) {
                sequenceList.add(low);
                low = myList.get(i);
                System.out.println("low " + low);
                sequenceList.add(low);
                System.out.println(sequenceList);
                listOfSequences.add(sequenceList);
                sequenceList = new ArrayList<>();
            }

        }
        if (myList.get(myList.size() - 3) < myList.get(myList.size() - 2) && myList.get(myList.size() - 2) > myList.get(myList.size() - 1)) {
            sequenceList.add(low);
            low = myList.get(myList.size() - 1);
            sequenceList.add(low);
            listOfSequences.add(sequenceList);
        }
        System.out.println(listOfSequences);

        List<Integer> longestBetonicSequence = new ArrayList<>();
        for (List<Integer> sequence : listOfSequences) {
            if (longestBetonicSequence.size() < sequence.size()) {
                longestBetonicSequence = sequence;
            }
        }
        System.out.println("The longest bitonic sequence is:" + longestBetonicSequence.toString());
    }

}


