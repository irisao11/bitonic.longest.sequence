package longest.betonic.sequence;

import java.util.Arrays;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        Integer[] myArray = { -1, 8, 4, 13, 17, 5, 1, 19, 4};
        List<Integer> myList = Arrays.asList(myArray);

        LongestBetonicSequence longestBetonicSequence = new LongestBetonicSequence();
        longestBetonicSequence.findLongestBetonicSequence(myList);
    }
}
